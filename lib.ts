/** A function that can be called using the `await` keyword */
type AwaitableFunction = (...args: any[]) => PromiseLike<any>;

/**
 * Infers the return type of the promise {@link T}.
 * 
 * See [source on StackOverflow](https://stackoverflow.com/a/49889856/238753) for implementation details.
 */
type PromiseResultType<T extends PromiseLike<any>> = T extends PromiseLike<infer U> ? U : T

type AsyncReturnType<T extends AwaitableFunction> = Promise<PromiseResultType<ReturnType<T>>>;

/**
 * Returns a function that calls {@link originalFunction} but with {@link preserveErrorStack} applied
 */
export function wrap<T extends AwaitableFunction>(originalFunction: T): (...args: Parameters<T>) => AsyncReturnType<T> {
    const wrappedFunction = async function(...args: Parameters<T>): AsyncReturnType<T> {
        const originalPromise = originalFunction(...args);
        const result = await preserveErrorStack(originalPromise, wrappedFunction);
        return result;
    }

    return wrappedFunction;
}

/**
 * Calls {@link promise}, but adds a more useful stack trace to any errors it throws.
 * The stack trace is modified to include the caller's async call stack.
 * 
 * This is similar to the [await-trace](https://www.npmjs.com/package/await-trace) library but with a nicer interface.
 * 
 * @param promise The promise to wrap
 * @param excludeFramesFrom
 * Stack frames in the resulting async stack trace will stop before this function.
 * Use this to hide implementation details in your wrapping code.
 */
 export async function preserveErrorStack<Result>(promise: PromiseLike<Result>, excludeFramesFrom: Function = preserveErrorStack): Promise<Result> {
    if (!(promise?.then instanceof Function)) {
        throw new TypeError('The value supplied for promise is not a promise');
    }

    if (!(excludeFramesFrom instanceof Function)) {
        throw new TypeError(`The value supplied for excludeFramesFrom is not a function`);
    }

    try {
        return await promise;
    } catch (e) {
        if (e instanceof Error) {
            const asyncCallStack = getCurrentCallStackPartial(excludeFramesFrom);
            const stacks = [e.stack, asyncCallStack];
            const nonEmptyStacks = stacks.filter(stack => stack);
            const newStack = nonEmptyStacks.join('\n');
            e.stack = newStack;
        }

        throw e;
    }
}

/**
 * Captures the current call stack, passing in {@link excludeFramesFrom} to {@link Error.captureStackTrace}.
 * The resulting call stack does not include the first line, [which normally includes the error type and message](https://nodejs.org/api/errors.html#errors_error_stack).
 */
function getCurrentCallStackPartial(excludeFramesFrom: Function) {
    const stackHolder: { stack?: string; } = {};
    Error.captureStackTrace(stackHolder, excludeFramesFrom);
    const stack = stackHolder.stack!;
    const trimmedStack = removeFirstLine(stack);
    return trimmedStack;
}

function removeFirstLine(string: string) {
    const firstNewLineIndex = string.indexOf('\n');
    if (firstNewLineIndex < 0) {
        // The string only consists of a single line, so the runtime wasn't able to put together an async stack trace
        // This can happen on old versions of Node/V8 that don't support zero-cost async stack traces
        return '';
    }

    const secondLineIndex = firstNewLineIndex + 1;
    return string.substring(secondLineIndex);
}
