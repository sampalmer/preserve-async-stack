import axios from 'axios';
import { createReadStream, createWriteStream, PathLike } from 'fs';
import * as fs from 'fs/promises';
import { preserveErrorStack, wrap } from './lib';

const axiosWrapped = wrap(axios);

async function main() {
    await inner();
}

async function inner() {
    return await readFile();
}

async function readFile() {
    // return await preserveErrorStack(new Promise((resolve, reject) => setTimeout(() => reject(new Error('oh-oh')), 1000)));
    // return await preserveErrorStack(fs.readFile('non-existing'));
    // return await preserveErrorStack(axios('https://www.example.com/asdf'));
    // return await axiosWrapped('https://www.example.com/asdf');
    return await preserveErrorStack(copyFile('non-existing', 'out.txt'));
    // return await preserveErrorStack(delayedCrash(500));
}

async function copyFile(source: PathLike, destination: PathLike) {
    return new Promise<void>((resolve, reject) => {
        createReadStream(source)
            .on('error', e => reject(e))
            .pipe(createWriteStream(destination))
            .on('error', e => reject(e))
            .on('finish', () => resolve());
    });
}

async function delayedCrash(millis: number) {
    return new Promise<void>((resolve, reject) => {
        setTimeout(() => reject(new Error('Crashing after delay')), millis);
    });
}

main().catch(e => {
    console.error(e?.stack ?? e);
});

