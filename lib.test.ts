import { preserveErrorStack } from "./lib";
import * as NonNativePromise from "promise";
import 'jest-extended';

describe("Doesn't lose original error metadata", () => {
    test('Preserves original error instance', async () => {
        const originalError = new Error();
        const result = preserveErrorStack(Promise.reject(originalError));
        await expect(result).rejects.toBe(originalError);
    });
    
    test('Preserves original error properties', async () => {
        const originalError = new Error('Original message');
        const customPropertyName = 'customProperty';
        const customPropertyValue = Symbol();
        (originalError as any)[customPropertyName] = customPropertyValue;
        
        const expectedProperties = {
            name: originalError.name,
            message: originalError.message,
            [customPropertyName]: customPropertyValue,
        };

        const result = preserveErrorStack(Promise.reject(originalError));
        await expect(result).rejects.toMatchObject(expectedProperties);
    });

    test('Preserves original error message in call stack', async () => {
        const originalMessage = 'original message';
        const originalError = new Error(originalMessage);
        const result = preserveErrorStack(Promise.reject(originalError));
        await expect(result).rejects.toHaveProperty('stack', expect.stringContaining(originalMessage));
    });

    test('Preserves custom error classes', async () => {
        class CustomError extends Error {}
        const originalError = new CustomError();
        const result = preserveErrorStack(Promise.reject(originalError));
        await expect(result).rejects.toBeInstanceOf(CustomError);
    });
    
    test('Preserves non-Error errors', async () => {
        const originalError = 'error string';
        const result = preserveErrorStack(Promise.reject(originalError));
        await expect(result).rejects.toBe(originalError);
    });
});

describe('Validates arguments', () => {
    test('Promise is mandatory', async () => {
        const result = preserveErrorStack(null!);
        await expect(result).rejects.toThrow(/promise|function|required|mandatory/i);
    });

    test('Promise must be a promise', async () => {
        const result = preserveErrorStack({} as Promise<void>);
        await expect(result).rejects.toThrow(/promise/i);
    });

    test('Supports promise-like', async () => {
        const promiseLike = NonNativePromise.resolve();
        const result = preserveErrorStack(promiseLike);
        await expect(result).toResolve();
    });

    test('Exclusion must be a function', async () => {
        const result = preserveErrorStack(Promise.resolve(), {} as Function);
        await expect(result).rejects.toThrow(/function/i);
    });
});

describe('Adds caller stack trace', () => {
    const originalErrorMessage = 'Original error message';
    const fakeStackLines = [
        `Error: ${originalErrorMessage}`,
        '    at REPL3:1:1',
        '    at Script.runInThisContext (node:vm:129:12)',
        '    at REPLServer.defaultEval (node:repl:562:29)',
        '    at bound (node:domain:421:15)',
        '    at REPLServer.runBound [as eval] (node:domain:432:12)',
        '    at REPLServer.onLine (node:repl:889:10)',
        '    at REPLServer.emit (node:events:402:35)',
        '    at REPLServer.emit (node:domain:475:12)',
        '    at REPLServer.Interface._onLine (node:readline:487:10)',
        '    at REPLServer.Interface._line (node:readline:864:8)',
    ];
    const fakeStack = fakeStackLines.join('\n');

    test('Call stack includes immediate caller', async () => {
        const originalError = new Error(originalErrorMessage);
        originalError.stack = fakeStack;

        async function immediateCallingFunction() {
            return await preserveErrorStack(Promise.reject(originalError));
        }

        await expect(immediateCallingFunction).rejects.toHaveProperty('stack', expect.stringContaining(immediateCallingFunction.name));
    });
  
    test(`Call stack doesn't include ${preserveErrorStack.name} function`, async () => {
        const originalError = new Error(originalErrorMessage);
        originalError.stack = fakeStack;

        const result = preserveErrorStack(Promise.reject(originalError));

        await expect(result).rejects.toHaveProperty('stack', expect.not.stringContaining(preserveErrorStack.name));
    });

    test(`Call stack doesn't include explicitly excluded function`, async () => {
        const originalError = new Error(originalErrorMessage);
        originalError.stack = fakeStack;

        async function immediateCallingFunction() {
            return await preserveErrorStack(Promise.reject(originalError), immediateCallingFunction);
        }

        await expect(immediateCallingFunction).rejects.toHaveProperty('stack', expect.not.stringContaining(immediateCallingFunction.name));
    });

    test(`Call stack doesn't duplicate original message`, async () => {
        const originalError = new Error(originalErrorMessage);
        originalError.stack = fakeStack;

        const result = preserveErrorStack(Promise.reject(originalError));

        await expect(result).rejects.toHaveProperty('stack', expect.toIncludeRepeated(originalErrorMessage, 1));
    });
    
    test(`Call stack doesn't duplicate error name`, async () => {
        const originalError = new Error(originalErrorMessage);
        originalError.stack = fakeStack;

        const result = preserveErrorStack(Promise.reject(originalError));

        await expect(result).rejects.toHaveProperty('stack', expect.toSatisfy(stack => stack.match(/^Error/mg).length === 1));
    });
    
    test(`Handles when original error is missing call stack`, async () => {
        const originalError = new Error(originalErrorMessage);
        delete originalError.stack;

        const result = preserveErrorStack(Promise.reject(originalError));

        await expect(result).rejects.toHaveProperty('stack', expect.toSatisfy(stack => !stack.startsWith(String(undefined))));
    });

    test(`Handles when calling stack is empty`, async () => {
        const originalError = new Error(originalErrorMessage);
        originalError.stack = fakeStack;
        
        const result = preserveErrorStack(Promise.reject(originalError));

        const originalPrepareStackTrace = Error.prepareStackTrace;
        try {
            Error.prepareStackTrace = (error) => `${error.name}: ${error.message}`;
            await expect(result).rejects.toHaveProperty('stack', fakeStack);
        } finally {
            Error.prepareStackTrace = originalPrepareStackTrace;
        }
    });
});

describe('Wraps original promise', () => {
    test('Resolves when original promise resolves', async () => {
        const result = preserveErrorStack(Promise.resolve());
        await expect(result).toResolve();
    });

    test('Returns original promise return value', async () => {
        const originalValue = Symbol();
        const result = preserveErrorStack(Promise.resolve(originalValue));
        await expect(result).resolves.toBe(originalValue);
    })
});
